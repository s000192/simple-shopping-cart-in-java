import java.math.BigDecimal;
import java.math.RoundingMode;

public class ShoppingCart {
    public static class Item {
        String name;
        int quantity;
    }

    public static class TaxRate {
        String location;
        BigDecimal rate;
    
        public static TaxRate[] list() {
            TaxRate[] list = new TaxRate[2];

            for (int i = 0; i < list.length; i++) {
                list[i] = new TaxRate();
            }

            list[0].location = "NY";
            list[0].rate = new BigDecimal("0.08875");
            list[1].location = "CA";
            list[1].rate = new BigDecimal("0.0975");
            return list;
        }
    
        public static BigDecimal checkByLocation(final String location) {
            int i = 0;
            TaxRate[] taxRateList = list();
            while (i < taxRateList.length) {
                if (taxRateList[i].location == location) {
                    return taxRateList[i].rate;
                }
                i++;
            }
            return new BigDecimal("0");
        }
    }

    public static class Stock {
        enum Category {
            BOOK, FOOD, STATIONARY, CLOTHING, UNKNOWN
        }

        String name;
        BigDecimal price;
        Category category;

        // change to hashtable
        public static Stock[] list() {
            Stock[] list = new Stock[4];
        
            for (int i = 0; i < list.length; i++) {
                list[i] = new Stock();
            }

            list[0].name = "book";
            list[0].category = Category.BOOK;
            list[0].price = new BigDecimal("17.99");
            list[1].name = "shirt";
            list[1].category = Category.CLOTHING;
            list[1].price = new BigDecimal("29.99");
            list[2].name = "potato chips";
            list[2].category = Category.FOOD;
            list[2].price = new BigDecimal("3.99");
            list[3].name = "pencil";
            list[3].category = Category.STATIONARY;
            list[3].price = new BigDecimal("2.99");
            return list;
        }
    }

    public static class UseCase {
        public static final String location1 = "CA";
        public static final Item[] shoppingCart1() {
            Item[] shoppingCart1 = new Item[2];
            for (int i = 0; i < shoppingCart1.length; i++) {
                shoppingCart1[i] = new Item();
            }

            shoppingCart1[0].name = "book";
            shoppingCart1[0].quantity = 1;
            shoppingCart1[1].name = "potato chips";
            shoppingCart1[1].quantity = 1;
            return shoppingCart1;
    }   

        public static final String location2 = "NY";
        public static final Item[] shoppingCart2(){ 
            Item[] shoppingCart2 = new Item[2];
            for (int i = 0; i < shoppingCart2.length; i++) {
                    shoppingCart2[i] = new Item();
                }

                shoppingCart2[0].name = "book";
                shoppingCart2[0].quantity = 1;
                shoppingCart2[1].name = "pencil";
                shoppingCart2[1].quantity = 3;
            return shoppingCart2;
        }

        public static final String location3 = "NY";
        public static final Item[] shoppingCart3() { 
            Item[] shoppingCart3 = new Item[2];
        
            for (int i = 0; i < shoppingCart3.length; i++) {
                shoppingCart3[i] = new Item();
            }

            shoppingCart3[0].name = "pencil";
            shoppingCart3[0].quantity = 2;
            shoppingCart3[1].name = "shirt";
            shoppingCart3[1].quantity = 1;
            return shoppingCart3;
        }
    }

    static Stock[] stocklist = Stock.list();
    static TaxRate[] taxRateList = TaxRate.list();

    private static Stock getItemInfoByName(final String name) {
        int i = 0;
        while (i < stocklist.length) {
            if (stocklist[i].name == name) {
                return stocklist[i];
            }
            i++;
        }
        final Stock emptyStock = new Stock();
        emptyStock.name = "";
        emptyStock.price = new BigDecimal("0");
        emptyStock.category = Stock.Category.UNKNOWN;
        return emptyStock;
    }

    private static BigDecimal calcSubtotal(final Item[] shoppingCart) {
        BigDecimal subtotal = new BigDecimal("0");
        for (int i = 0; i < shoppingCart.length; i++) {
            final String itemName = shoppingCart[i].name;
            final int quantity = shoppingCart[i].quantity;
            BigDecimal price = getItemInfoByName(itemName).price;
            BigDecimal costForCurrentItem = BigDecimal.valueOf(quantity).multiply(price);
            subtotal = subtotal.add(costForCurrentItem);
        }
        return subtotal;
    }

    private static BigDecimal taxRoundUp(BigDecimal tax) {
        BigDecimal roundingFactor = new BigDecimal("20.0");
        BigDecimal before = tax.multiply(roundingFactor);
        if (before.remainder(new BigDecimal("1")) == new BigDecimal("0")) {
            before = before.setScale(0, RoundingMode.CEILING);
        }
        before = before.setScale(0, RoundingMode.FLOOR).add(new BigDecimal("1"));
        BigDecimal taxRoundedUp = before.divide(roundingFactor);
        return taxRoundedUp;
    }

    private static BigDecimal calcTax(final Item[] shoppingCart, final String location) {
        BigDecimal tax = new BigDecimal("0");
        BigDecimal taxRate = TaxRate.checkByLocation(location);

        for (int i = 0; i < shoppingCart.length; i++) {
            final String itemName = shoppingCart[i].name;
            final int quantity = shoppingCart[i].quantity;
            final Stock itemInfo = getItemInfoByName(itemName);
            final BigDecimal price = itemInfo.price;
            final Stock.Category category = itemInfo.category;
            if ((location == "NY" && category != Stock.Category.FOOD && category != Stock.Category.CLOTHING) || ((location == "CA" && category != Stock.Category.FOOD))) {
                BigDecimal costForCurrentItem = BigDecimal.valueOf(quantity).multiply(price);
                BigDecimal taxForCurrentItem = costForCurrentItem.multiply(taxRate);
                tax =  tax.add(taxForCurrentItem);
            }
        }
        tax = taxRoundUp(tax);
        return tax;
    }

    public static class Logger {

        private static final int MAX_WHITESPACES = 15;

        public static void s(String a, String b, String c) {
            String whitespaces1 = new String(new char[Math.abs(a.length()-MAX_WHITESPACES)]).replace('\0', ' ');
            String whitespaces2 = new String(new char[Math.abs(b.length()-MAX_WHITESPACES)]).replace('\0', ' ');
            String line = String.format("%s"+whitespaces1+"%s"+whitespaces2+"%s", a, b, c);
            System.out.println(line);
        }

        public static void t(String a, String b) {
            String whitespaces1 = new String(new char[Math.abs(a.length()-29)]).replace('\0', ' ');
            String line = String.format("%s"+whitespaces1+"%s", a, b);
            System.out.println(line);
        }

    }

    private static void printReceipt(final Item[] shoppingCart, final String location) {
        BigDecimal subtotal = calcSubtotal(shoppingCart);
        BigDecimal tax = calcTax(shoppingCart, location);
        BigDecimal total = subtotal.add(tax);

        String titleTemplate = "%-10s %10s %10s%n";
        System.out.printf(titleTemplate, "item", "price", "qty");

        String template = "%-10s %10s %10s%n";
        for (int i = 0; i < shoppingCart.length; i++) {
            String itemName = shoppingCart[i].name;
            String quantity = String.valueOf(shoppingCart[i].quantity);
            String price = getItemInfoByName(itemName).price.toString();
            System.out.printf(template, itemName, price, quantity);
        }

        String calcTemplate = "%-15s %16s%n";
        System.out.printf(calcTemplate, "subtotal:", String.format("%.2f", subtotal));
        System.out.printf(calcTemplate, "tax:", String.format("%.2f", tax));
        System.out.printf(calcTemplate, "total:", String.format("%.2f", total));
    }

    public static void main(final String[] argc) {
        printReceipt(UseCase.shoppingCart1(), UseCase.location1);
        System.out.println("");
        printReceipt(UseCase.shoppingCart2(), UseCase.location2);
        System.out.println("");
        printReceipt(UseCase.shoppingCart3(), UseCase.location3);
        System.out.println("");
    }
}